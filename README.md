30.350 — CNFed., sala I civil y com., agosto 12-977. — Martínez, Carlos R, A c. Gobierno Nacional, 2da Parte

Empero, como el propio recurrente lo manifiesta en el escrito de fs. 395/400, ya retiró en concepto de honorarios la suma de $99.198 en el mes de julio de 1975, la cual, actualizada, conforme con el criterio ya indicado, , llega a $572.372. Restada del total indicado precedentemente, llegase así a $11.464.501.

En virtud de lo expuesto, el tribunal resuelve regular el honorario del letrado apoderado de la actora, doctor Ernesto Marce-naro Boutell por su actuación extrajudicial y la realizada en autos hasta la sentencia de primera instancia, en la suma de pesos 11.464.501.

Y por la gestión en segunda instancia con motivo del recurso interpuesto a fs. 137, concedido a fs. 139 y resuelto a fs. 163/ 166, habida cuenta el interés disputado y el resultado de la apelación, se regulan al citado profesional $2.407.545 (art. 11 y 79 del citado arancel).

El doctor Jorge O. Arana Tagle no suscribe la presente en virtud de haberse aceptado su excusación a fs. 401. — Jorge G. Pérez Delgado. — Roberto M. Muzio (Sec.: Mas ría del C. Jeanneret de Pérez Cortés).
ABOGADO: Honorarios; monto del juicio por expropiación de acciones. MONEDA: Depreciación; pautas para su cómputo; expropiación. INGENIEROS Y ARQUITECTOS: Honorarios.

1.— No existiendo en autos pautas numéricas concretas que sean susceptibles de tomar como base para precisar el monto del juicio por expropiación de acciones que fue desistido antes de producirse la prueba pericial contable, es imprescindible acudir al prudente arbitrio judicial como criterio para la regulación de honorarios, al cual no es ajeno la ponderación del interés económico controvertido, en función de lo que es el objeto de la demanda (en el caso, se tomaron en cuenta las siguientes circunstancias:diferentes estimaciones formuladas por las partes, y situación de la sociedad, destacándose el criterio propuesto por los profesionales consistente en actualizar el valor de los bienes inmuebles y no, en cambio, el pasivo de la sociedad).

2.—  Ante la constancia y gravedad del proceso inflacionario, a los efectos del cómputo de la depreciación monetaria cabe inclinarse por una mayor y más cabe indicar o nación . 103 marces elaborados por el Instituto Nacional de Estadística y Censos, oren que considerados de modo global y en tanto sirvan como reflejo de nuestra realidad económica, aunque, claro está, sin dejar de tener en cuenta que aquéllos no deben aplicarse automáticamente, pues corresponde considerar todas las circunstancias que especifican el caso, haciendo mérito de una prudente interpretación de la realidad económica.

3.—  En materia expropiatoria y a los fines del cómputo de la depreciación monetaria no deben tomarse estrictamente los índices oficiales del Instituto Nacional de Estadística y Censos, ya que no todos los bienes siguen una curva pareja de incremento, debiendo ponderarse la naturaleza de aquel de que se trata.

4.—  En los procesos de expropiación no rigen estrictamente las normas del arancel de abogados y procuradores, por lo cual sus disposiciones deben ser aplicadas sólo como pautas orientadoras.

5.— La actuación del representante de los expropiados ante el Tribunal de Tasaciones no es necesariamente equiparable, respecto de los honorarios, a la de los peritos, por lo cual no es imperativo que la regulación de sus honorarios se practique con arreglo al arancel pertinente.

